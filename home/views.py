from django.shortcuts import render
from django.shortcuts import render_to_response
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.template.context import RequestContext
# Create your views here.


def login(request):
    if request.method == 'GET':
        return render(request, 'home\login.html.j2')
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            auth.login(request, user)
            return HttpResponseRedirect("/")
        else:
            return render(request, 'home\login.html.j2', {'password_is_wrong':True})

@login_required()
def logout(request):
    auth.logout(request)
    return HttpResponseRedirect("/login/")

@login_required(redirect_field_name='',login_url='/login/')
def index(request):
    list = [
        (1001, 'Lorem ', 'ipsum ', 'dolor ', 'sit'),
        (1002, 'amet ', 'consectetur ', 'adipiscing ', 'elit'),
        (1003, 'Integer ', 'nec ', 'odio ', 'Praesent'),
        (1003, 'libero ', 'Sed ', 'cursus ', 'ante'),
        (1004, 'dapibus ', 'diam ', 'Sed ', 'nisi'),
        (1005, 'Nulla ', 'quis ', 'sem ', 'at'),
        (1006, 'nibh ', 'elementum ', 'imperdiet ', 'Duis'),
        (1007, 'sagittis ', 'ipsum ', 'Praesent ', 'mauris'),
        (1008, 'Fusce ', 'nec ', 'tellus ', 'sed'),
        (1009, 'augue ', 'semper ', 'porta ', 'Mauris'),
        (1010, 'massa ', 'Vestibulum ', 'lacinia ', 'arcu'),
        (1011, 'eget ', 'nulla ', 'Class ', 'aptent'),
        (1012, 'taciti ', 'sociosqu ', 'ad ', 'litora'),
        (1013, 'torquent ', 'per ', 'conubia ', 'nostra'),
        (1014, 'per ', 'inceptos ', 'himenaeos ', 'Curabitur'),
        (1015, 'sodales ', 'ligula ', 'in ', 'libero')
    ]
    list1 = {'1': 1, '2': 2, '3': 3, '4': 4}
    return render(request, 'home\index.html.j2', {'list': list, 'list1': list1})
